#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. superlib
. "${SCRIPTPATH}/deploylib"

#=================================================

# deploy1/rootfs-strap * GPLv3 (C) 2021 librehof.com

summary()
{
  info ""
  info "Strap mounted rootfs: chroot host/strap + pubkey"
  info ""
  info "Usage:"
  info " ${SCRIPT} <hostdir> <rootdir> <storage> new|update [pubfile]"
  info ""
  info "Mount: /mnt/squashfs"
  info ""
  info "Run after rootfs-stage and rootfs-bootmounts"
  info "Do not bind rootfs"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

checksuperuser
exitonerror ${?}

HOSTDIR="${1}"
ROOTDIR="${2}"
STORAGE="${3}"
METHOD="${4}"
PUBFILE="${5}"

if [ "${HOSTDIR}" = "" ] || [ "${ROOTDIR}" = "" ] || [ "${STORAGE}" = "" ] || [ "${METHOD}" = "" ]; then
  inputexit
fi

if [ "${6}" != "" ]; then
  inputexit
fi

if [ "${METHOD}" != "new" ] && [ "${METHOD}" != "update" ]; then
  inputexit
fi

HOSTDIR="$(abspath "${HOSTDIR}")"
exitonerror ${?}

checkdir "${HOSTDIR}"
exitonerror ${?}

ROOTDIR="$(abspath "${ROOTDIR}")"
exitonerror ${?}

checkdir "${ROOTDIR}"
exitonerror ${?}

rootfs-release "${ROOTDIR}"

dirmount-remove "/mnt/squashfs"

if [ "${PUBFILE}" != "" ]; then
  PUBFILE="$(abspath "${PUBFILE}")"
  exitonerror ${?}
  PUBKEY="$(loadline "${PUBFILE}")"
  exitonerror ${?}
  if [ "${PUBKEY}" = "" ]; then
    errorexit "Blank pubfile: ${PUBFILE}"
  fi
fi

#-------------------------------------------------

loadconf "host" "${STORAGE}" "${HOSTDIR}"
exitonerror ${?}

#-------------------------------------------------

if [ "${PUBKEY}" != "" ]; then
  ensurecopy "${PUBFILE}" "${ROOTDIR}/root/admin.pub"
  exitonerror ${?}
fi

#-------------------------------------------------

if [ "${METHOD}" = "new" ] && [ -f "${HOSTDIR}/strap" ]; then

  note "[inner strap]"
  sleep 2

  rootfs-bind "${ROOTDIR}" resolv
  exitonerror ${?}

  rootfs-run "${ROOTDIR}" "/deploy/strap"
  exitonerror ${?}

  rootfs-release "${ROOTDIR}"
  exitonerror ${?}

fi

if [ "${METHOD}" = "new" ] && [ -f "${HOSTDIR}/sync" ]; then

  note "[sync push initial]"
  sleep 1

  rootfs-run "${ROOTDIR}" "/deploy/sync push initial"
  if [ "${?}" != 0 ]; then
    note "Failed, trying again ..."
    sleep 1
    rootfs-run "${ROOTDIR}" "/deploy/sync push initial"
    if [ "${?}" != 0 ]; then
      note "Failed to complete /deploy/sync"
      info "Press ENTER to continue, CTRL-C to abort"
      keywait
    fi
  fi

fi

#chmod 600 "${ROOTDIR}/deploy"
#exitonerror ${?}

#-------------------------------------------------

if [ "${NETWORK}" = "xip" ] && [ "${LANIP}" != "" ]; then
  if [ "${MACHINE}" = "" ]; then
    HOSTPATH="${HOST}"
  else
    HOSTPATH="${MACHINE}/${HOST}"
  fi
  note "Review x-ip network for ${HOSTPATH} (${LANIP})"
  info "qemu/kvm example:"
  info " virsh net-edit default"
  info "   <host name='<hostname>' ip='${LANIP}'/>"
  info " virsh net-destroy default && virsh net-start default"
  keywait 4
fi

#-------------------------------------------------

# admin home (must be created by inner stage if not root)

if [ ! -d "${ROOTDIR}${ADMINDIR}" ]; then
  errorexit "Missing ${ROOTDIR}${ADMINDIR}"
fi

if [ "${PUBKEY}" != "" ]; then
  ensuredir "${ROOTDIR}${ADMINDIR}/.ssh"
  exitonerror ${?}
  ensurefile "${ROOTDIR}${ADMINDIR}/.ssh/authorized_keys"
  exitonerror ${?}
  FOUND="$(grep "${PUBKEY}" "${ROOTDIR}${ADMINDIR}/.ssh/authorized_keys")"
  if [ "${FOUND}" = "" ]; then
    action "Adding ssh key to ${ADMINDIR}/.ssh/authorized_keys"
    appendline "${PUBKEY}" "${ROOTDIR}${ADMINDIR}/.ssh/authorized_keys"
    exitonerror ${?}
  fi
  chmod 700 "${ROOTDIR}${ADMINDIR}/.ssh"
  chmod 600 "${ROOTDIR}${ADMINDIR}/.ssh/authorized_keys"
fi

#-------------------------------------------------

ensurenofile "${ROOTDIR}/run/superpm.day"
exitonerror ${?}
