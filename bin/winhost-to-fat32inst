#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. superlib
. "${SCRIPTPATH}/deploylib"

#=================================================

# deploy1/winhost-to-fat32inst * GPLv3 (C) 2021 librehof.com

summary()
{
  info ""
  info "Make winhost USB installer (UEFI fat32 disk or image)"
  info ""
  info "Usage:"
  info " ${SCRIPT} <hostdir> <diskdev> [<mibs>]"
  info " ${SCRIPT} <hostdir> <diskimage> [<mibs>]"
  info " ${SCRIPT} <hostdir> xdisk [<mibs>]"
  info " ${SCRIPT} cleanup"
  info ""
  info "hosts/<host>/"
  info " winhost.conf"
  info "vhosts/<host>/"
  info " winhost.conf"
  info ""
  info "xdisk => xdev/<HOST>.usb (installation usb image)"
  info ""
  info "Mounts:"
  info " /mnt/wininst"
  info " /mnt/fat32inst"
  info ""
  info "Will generate autounattend.xml based on config"
  info ""
  info "Default FAT32 partition is 7700 MiB (mibs)"
  info ""
  info "When using with QEMU/KVM:"
  info " host disk: SATA"
  info " fat32inst disk: USB (boot, install, delete)"
  info ""
  info "Examples:"
  info " ${SCRIPT} hosts/somehost /dev/sdx"
  info " ${SCRIPT} hosts/somehost xdisk"
  info ""
}

#=================================================

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

# check base
checkdir "artifacts"
exitonerror ${?}

checksuperuser
exitonerror ${?}

if [ "${1}" = "cleanup" ] && [ "${2}" = "" ]; then
  dirmount-remove "/mnt/wininst"
  dirmount-remove "/mnt/fat32inst"
  "${SCRIPTPATH}/xdisk-detach" "all"
  exit 0
fi

#-------------------------------------------------

# input

HOSTDIR="${1}"
TARGET="${2}"
MIBS="${3}"

if [ "${HOSTDIR}" = "" ] || [ "${TARGET}" = "" ]; then
  inputexit
fi

if [ -e "/mnt/wininst" ]; then
  errorexit "Already mounted: /mnt/wininst (cleanup)"
fi

if [ -e "/mnt/fat32inst" ]; then
  errorexit "Already mounted: /mnt/fat32inst (cleanup)"
fi

HOSTDIR="$(abspath "${HOSTDIR}")"
exitonerror ${?}

checkdir "${HOSTDIR}"
exitonerror ${?}

checkfile "${HOSTDIR}/autounattend.xml"
exitonerror ${?}

if [ "${MIBS}" = "" ]; then
  MIBS=7700
fi

checkint "${MIBS}" space
exitonerror ${?}

#-------------------------------------------------

len="$(stringlen "${TARGET}")"
devdir="$(startstring "${TARGET}" 5)"
xdir="$(dirname "${TARGET}")"
basedir="$(dirname "${xdir}")"

if [ "${devdir}" = "/dev/" ] || [ -b "${TARGET}" ]; then
  STORAGE="disk"
elif [ "${TARGET}" = "xdisk" ]; then
  STORAGE="xdisk"
else
  STORAGE="vdisk"
fi

# pre-load winhost.conf (without wininst.conf)
loadconf "winhost" "${STORAGE}" "${HOSTDIR}"
exitonerror ${?}

LABEL="$(upperstring "${HOST}")"

ISOFILE="artifacts/${WININST}.wininst.iso"

ISOFILE="$(abspath "${ISOFILE}")"
exitonerror ${?}

checkfile "${ISOFILE}"
exitonerror ${?}

#-------------------------------------------------

if [ "${STORAGE}" = "xdisk" ]; then
  ensuredir "${XDISKS}"
  exitonerror ${?}
  TARGET="${XDISKS}/${LABEL}.usb"
  if [ ! -f "${TARGET}" ]; then
    action "Creating ${TARGET}"
  else
    action "Overwriting ${TARGET}"
    info "Press ENTER to continue, CTRL-C to abort"
    keywait
    image-detach "${TARGET}"
    exitonerror ${?}
    ensurenofile "${TARGET}"
    exitonerror ${?}
  fi
  image-create "${TARGET}" $((MIBS+10))M
  exitonerror ${?}
  ATTACH="image"
  DISKDEV="$(image-attach "${TARGET}")"
  exitonerror ${?}
elif [ "${STORAGE}" = "vdisk" ]; then
  TARGET="$(abspath "${TARGET}")"
  len="$(stringlen "${TARGET}")"
  disk="$(endstring "${TARGET}" $((len-4)))"
  raw="$(endstring "${TARGET}" $((len-3)))"
  if [ "${disk}" = ".disk" ] || [ "${raw}" = ".raw" ] || [ "${raw}" = ".usb" ]; then
    ATTACH="image"
    DISKDEV="$(image-attach "${TARGET}" reuse)"
    exitonerror ${?}
  else
    ATTACH="vimage"
    DISKDEV="$(vimage-attach "${TARGET}" unsafe)"
    exitonerror ${?}
  fi
else
  ATTACH=""
  TARGET="$(abspath "${TARGET}")"
  DISKDEV="${TARGET}"
fi

checkdevice "${DISKDEV}"
exitonerror ${?}

#-------------------------------------------------

div

note "HOSTDIR:  ${HOSTDIR} (${WININST})"
note "TARGET:   ${TARGET} (${VARIANT})"
note "MIBS:     ${MIBS}"
note "LABEL:    ${LABEL}"
note "ISOFILE:  ${ISOFILE}"

div

info "Press ENTER or wait for 10 seconds"
keywait 10

#-------------------------------------------------
separator "ISO"

checkfile "${ISOFILE}"
exitonerror ${?}

ISODEV="$(image-attach "${ISOFILE}")"
exitonerror ${?}

note "ISO loop device: ${ISODEV}"
sleep 1

dirmount-iso "/mnt/wininst" "${ISODEV}" ro
exitonerror ${?}

if [ ! -f "/mnt/wininst/sources/install.esd" ]; then
  errorexit "Missing /mnt/wininst/sources/install.esd"
fi

note "Found sources/install.esd"

#-------------------------------------------------
separator "Format"

disk-format "${DISKDEV}" quick mbr
exitonerror ${?}

PARTDEV="$(diskpart-add "${DISKDEV}" 1 4M ${MIBS}M fat32)"
exitonerror ${?}

LABEL11="$(startstring "${LABEL}" 11)"
format-fat32 "${PARTDEV}" "${LABEL11}"
exitonerror ${?}

diskpart-flag "${DISKDEV}" 1 boot on
exitonerror ${?}

#-------------------------------------------------
separator "Copy"

dirmount-fat "/mnt/fat32inst" "${PARTDEV}"
exitonerror ${?}

dir-replicate "/mnt/wininst" "/mnt/fat32inst"
exitonerror ${?}

div

checkfile "/mnt/fat32inst/wininst.conf"
exitonerror ${?}

# load winhost.conf (backed by wininst.conf)
loadconf "winhost" "${STORAGE}" "${HOSTDIR}" "/mnt/fat32inst/wininst.conf"
exitonerror ${?}

WINHOST="${HOST}"

note "WINHOST:   ${WINHOST}"
note "WINMIBS:   ${WINMIBS}"
note "WINUSER:   ${WINUSER}"
note "WINPASS:   ${WINPASS}"
note "WINLANG:   ${WINLANG}"
note "WINFORMAT: ${WINFORMAT}"
note "WININPUT:  ${WININPUT}"
note "WINKEY:    ${WINKEY}"

div

info "Press ENTER or wait for 10 seconds"
keywait 10

#-------------------------------------------------

# generate autounattend.xml

UNATTENDED="/mnt/fat32inst/autounattend.xml"
TEMPLATE="${HOSTDIR}/autounattend.xml"

action "Compiling autounattend.xml"
sleep 1

cp "${TEMPLATE}" "${UNATTENDED}"
exitonerror ${?}

replaceinfile '${WINHOST}' "${WINHOST}" "${UNATTENDED}"
replaceinfile '${WINUSER}' "${WINUSER}" "${UNATTENDED}"
replaceinfile '${WINPASS}' "${WINPASS}" "${UNATTENDED}"
replaceinfile '${WINLANG}' "${WINLANG}" "${UNATTENDED}"
replaceinfile '${WINFORMAT}' "${WINFORMAT}" "${UNATTENDED}"
replaceinfile '${WININPUT}' "${WININPUT}" "${UNATTENDED}"
replaceinfile '${WINMIBS}' "${WINMIBS}" "${UNATTENDED}"
replaceinfile '${WINKEY}' "${WINKEY}" "${UNATTENDED}"

# cleanup

ensurenofile "/mnt/fat32inst/wininst.conf"
exitonerror ${?}

#-------------------------------------------------
separator "Unmount"

dirmount-remove "/mnt/wininst"
image-detach "${ISODEV}"

dirmount-remove "/mnt/fat32inst"

if [ "${ATTACH}" = "image" ]; then
  image-detach "${TARGET}"
elif [ "${ATTACH}" = "vimage" ]; then
  vimage-detach "${DISKDEV}"
fi

#-------------------------------------------------
