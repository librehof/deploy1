#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. superlib
. "${SCRIPTPATH}/deploylib"

#=================================================

# deploy1/host-newid * GPLv3 (C) 2021 librehof.com

summary()
{
  info ""
  info "Create host (and root) ssh keys"
  info ""
  info "Usage:"
  info " ${SCRIPT} <hostdir> [root]"
  info ""
  info "Use secret-open first"
  info "or mkdir <hostdir>.secret"
  info ""
  info "Generated key files:"
  info ""
  info " <hostdir>.secret/"
  info "  rootfs.<variant>/"
  info "   etc/ssh/"
  info "    ssh_host_ed25519_key"
  info "   root/.ssh/"
  info "    id_ed25519"
  info ""
  info " <hostdir>/"
  info "  rootfs.<variant>/"
  info "   etc/ssh/"
  info "    ssh_host_ed25519_key.pub"
  info "   root/.ssh/"
  info "    id_ed25519.pub"
  info "  rootfs.<xvariant>/"
  info "   etc/ssh/"
  info "    ssh_host_ed25519_key"
  info "    ssh_host_ed25519_key.pub"
  info "   root/.ssh/"
  info "    id_ed25519"
  info "    id_ed25519.pub"
  info ""
}

#=================================================

# input

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

# check base
checkdir "artifacts"
exitonerror ${?}

HOSTDIR="${1}"

if [ "${HOSTDIR}" = "" ] || [ "${3}" != "" ]; then
  inputexit
fi

HOSTDIR="$(abspath "${HOSTDIR}")"
exitonerror ${?}

checkdir "${HOSTDIR}"
exitonerror ${?}

if [ ! -d "${HOSTDIR}.secret" ]; then
  info "Create secret directory with: bin/secret-open ${HOSTDIR}"
  errorexit "Missing ${HOSTDIR}.secret"
fi

loadcommon
exitonerror ${?}

HOST="$(basename "${HOSTDIR}")"

#-------------------------------------------------

# real

if [ "${VARIANT}" != "" ]; then

  # host
  PUBPATH="${HOSTDIR}/rootfs.${VARIANT}/etc/ssh"
  SECPATH="${HOSTDIR}.secret/rootfs.${VARIANT}/etc/ssh"
  if [ ! -f "${PUBPATH}/ssh_host_ed25519_key.pub" ]; then
    div
    action "Creating ${VARIANT} host key"
    mkdir -p "${PUBPATH}"
    exitonerror ${?} "Failed to create ${PUBPATH}"
    mkdir -p "${SECPATH}"
    exitonerror ${?} "Failed to create ${SECPATH}"
    ssh-keygen -f "${SECPATH}/ssh_host_ed25519_key" -N '' -t ed25519 -C "${HOST}"
    exitonerror ${?} "Failed to generate ${SECPATH}/ssh_host_ed25519_key"
    mv "${SECPATH}/ssh_host_ed25519_key.pub" "${PUBPATH}/"
    exitonerror ${?} "Failed to move ${SECPATH}/ssh_host_ed25519_key.pub"
  elif [ ! -f "${SECPATH}/ssh_host_ed25519_key" ]; then
    note "Found: ${PUBPATH}/ssh_host_ed25519_key.pub"
    errorexit "Missing: ${SECPATH}/ssh_host_ed25519_key"
  fi

  # superuser
  if [ "${2}" = "root" ]; then
    PUBPATH="${HOSTDIR}/rootfs.${VARIANT}/root/.ssh"
    SECPATH="${HOSTDIR}.secret/rootfs.${VARIANT}/root/.ssh"
    if [ ! -f "${PUBPATH}/id_ed25519.pub" ]; then
      div
      action "Creating ${VARIANT} root key"
      mkdir -p "${PUBPATH}"
      exitonerror ${?} "Failed to create ${PUBPATH}"
      mkdir -p "${SECPATH}"
      exitonerror ${?} "Failed to create ${SECPATH}"
      ssh-keygen -f "${SECPATH}/id_ed25519" -t ed25519 -C "root@${HOST}"
      exitonerror ${?} "Failed to generate ${SECPATH}/id_ed25519"
      mv "${SECPATH}/id_ed25519.pub" "${PUBPATH}/"
      exitonerror ${?} "Failed to move ${SECPATH}/id_ed25519.pub"
    elif [ ! -f "${SECPATH}/id_ed25519" ]; then
      note "Found: ${PUBPATH}/id_ed25519.pub"
      errorexit "Missing: ${SECPATH}/id_ed25519"
    fi
  fi

fi

#-------------------------------------------------

# experimental

if [ "${XVARIANT}" != "" ] && [ "${VARIANT}" != "${XVARIANT}" ]; then

  # host
  PUBPATH="${HOSTDIR}/rootfs.${XVARIANT}/etc/ssh"
  if [ ! -f "${PUBPATH}/ssh_host_ed25519_key.pub" ]; then
    div
    action "Creating ${XVARIANT} host key"
    mkdir -p "${PUBPATH}"
    exitonerror ${?} "Failed to create ${PUBPATH}"
    ssh-keygen -f "${PUBPATH}/ssh_host_ed25519_key" -N '' -t ed25519 -C "${HOST}"
    exitonerror ${?} "Failed to generate ${PUBPATH}/ssh_host_ed25519_key"
  elif [ ! -f "${PUBPATH}/ssh_host_ed25519_key" ]; then
    note "Found: ${PUBPATH}/ssh_host_ed25519_key.pub"
    errorexit "Missing: ${PUBPATH}/ssh_host_ed25519_key"
  fi

  # superuser
  if [ "${2}" = "root" ]; then
    PUBPATH="${HOSTDIR}/rootfs.${XVARIANT}/root/.ssh"
    if [ ! -f "${PUBPATH}/id_ed25519.pub" ]; then
      div
      action "Creating ${XVARIANT} root key"
      mkdir -p "${PUBPATH}"
      exitonerror ${?} "Failed to create ${PUBPATH}"
      ssh-keygen -f "${PUBPATH}/id_ed25519" -t ed25519 -C "root@${HOST}"
      exitonerror ${?} "Failed to generate ${PUBPATH}/id_ed25519"
    elif [ ! -f "${PUBPATH}/id_ed25519" ]; then
      note "Found: ${PUBPATH}/id_ed25519.pub"
      errorexit "Missing: ${PUBPATH}/id_ed25519"
    fi
  fi

fi

#-------------------------------------------------

div
note "TODO: bin/secret-close ${HOSTDIR} save"
