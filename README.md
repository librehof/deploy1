## Deploy 1

Lightweight Host Deployment - [Legal Notice](NOTICE)

GPLv3 (C) 2021 [librehof.com](http://librehof.com) 


### Initial setup
```
# 1. Make a new deployment repo on your git server

# 2. Clone your new repo into <yournewrepo>
```
```
# 3. Clone deploy1 inside <yournewrepo>

cd <yournewrepo>
git clone --depth 2 https://gitlab.com/librehof/deploy1.git
```
```
# 4. Setup default (and install supersys system-wide)

deploy1/init
```
```
# 5. Push initial files to your git server

# 6. Copy a generic host (as a beginner)

dir-replicate generic/<somehost> hosts/<yourhost>
```

### Supersys 
- **supersys** will be installed system-wide on your computer and all deployed linux hosts
- more info at [librehof.com](http://librehof.com) =\> supersys1 =\> commands 
- all commands will print help if `-h` is given


### Environment classes

- **real**
  - STORAGE=disk|vdisk
  - disk devices or virtual disk images 
  - default VARIANT="**live**" (final live environment)
- e**x**perimental
  - STORAGE=xdisk
  - virtual disk images under x\<variant\>/
  - default VARIANT="**dev**" (local development environment) 


### Config files

- config/default.conf
- config/workspace.conf # default.conf override (not in git)
- config/default.\<variant\>
- config/workspace.\<variant\> # default.\<variant\> override (not in git)
- machines/\<machine\>/machine.conf
- machines/\<machine\>/machine.\<variant\>
- \[v\]hosts/\<host\>/\[win\]host.conf
- \[v\]hosts/\<host\>/\[win\]host.\<variant\>

Loaded in the above order, see later sections for variable descriptions


### Networking

- generic rules:
  - Hosts access each other via domain name (DN)
  - The exception is when an outside operator connects into dev network via LANIP/PUBIP/SSHIP 
  - If hosts have LANIP, it is assumed they all sit on the same LAN (including operator)
  - A host can have 0..n local domain names, see LANDNLIST
  - A host can have 0..n public domain names, see PUBDNLIST
  - Blank LANDNLIST, LANIP and PUBDNLIST, gives default LANDNLIST="\<host\>"
  - Blank LANDNLIST + given LANIP, gives default LANDNLIST="\<host\>"
- accessibility:
  - **lan**: A local host (blank PUBDNLIST) can reach all other hosts
  - **gateway**: A local+public host can also reach all other hosts
  - **nolan**: A "cloud" host (blank LANDNLIST/LANIP) can only reach public domain names (nolan/gateway)
    - with "port forwarding" on the gateway, local hosts can be selectively exposed 


### Disk Layouts (partititioning/formatting)

- **ext4efi** - single-boot
  - \[ fat32: `single <host> efi entry` \]
  - \[ ext4: `single <host> rootfs with /boot (/swapfile)` \]
- **multiefi/btrfs** - single/multi-boot
  - \[ ... \]
  - \[ fat32: `...` `<host> efi entry` `...` \] 
  - \[ ... \]
  - \[ btrfs: `(/swapfile)` `/<host> rootfs` `/<host> rootfs` `...` \]
  - \[ ext2: `<host> /boot` \]
  - \[ ext2: `<host> /boot` \] 
  - \[ ... \]
- **multiefi/luksbtrfs** - single/multi-boot, password encrypted-at-rest
  - \[ ... \]
  - \[ fat32: `...` `<host> efi entry` `...` \] 
  - \[ ... \]
  - **\[ luks:** \[ btrfs: `(/swapfile)` `/<host> rootfs` `/<host> rootfs` `...` \] **\]**
  - \[ ext2: `<host> /boot` \]
  - \[ ext2: `<host> /boot` \] 
  - \[ ... \]


### File Structure (your deployment directory)
`[*]` = mandatory
```
- <your environment>/ # deployer base (your git repo)

  - archive/        # backups
  
  - artifacts/      # [*] backup to a mass-storage area, not in git!
    - <installer-ver-arch>.iso
    - <winverarchtype>.wininst.iso
    - <distroverarchtype>.rootfs.sqfs (squashfs)
    - <custom files>
    
  - config/         # [*] common config (part of your git repo)  
    - default.conf      # default [X]VARIANT
    - workspace.conf    # default.conf override (not in git)
    - default.<variant> # variant specific (must not set [X]VARIANT)
    - workspace.<variant> # default.<variant> override (not in git)
    - netlan.<variant>  # "lan/gateway" network plan (append to /etc/hosts)
    - netpub.<variant>  # "nolan" network plan (append to /etc/hosts)

  - models/         # inventory (part of your git repo)
    - <model>         # one model
      - manual.pdf      # hardware manual
              
  - machines/       # inventory (part of your git repo)
    - <machine>       # one machine
      - machine.conf    # [*] <see later section>
      - machine.<variant>
  
  - hosts/          # your hosts (part of your git repo)
    - <host>/         # one linux host, goes to /deploy
      - rootfs/         # common overlay on-top-of rootfs
      - rootfs.<variant>/ # <variant> overlay on-top-of common
      - rootfs.7z       # goes to /deploy.secret/rootfs (AES-encrypted)
      - rootfs.<variant>.7z  # goes to /deploy.secret/rootfs.<variant>
      - host.conf       # [*] <see later section>
      - host.<variant>  # variant override
      - stage           # custom base-to-rootfs copy before strap
      - strap           # [*] "chroot" setup (admin, ssh, ...)
      - strap.list      # packages to install during strap (see sw-include)
      - strapend.list   # additional packages to install during strap
      - strapend        # additonal strap, called by strap (inline script)
      - boot            # custom bootloader setup after strap (inline script)
      - setup           # setup [initial] - "on-host" setup (services, ...)
      - setup.list      # packages to install during setup (see sw-include)
      - sync            # <see later section>
      - syncend         # called by sync if not pull or status (inline script)
    - <winhost>/      # one windows host (installer)
      - winhost.conf    # [*] <see later section>
      - winhost.<variant> # variant override
      - autounattend.xml # autounattend template (pick from generic)
      - checklist.md    # manual post-installation steps
    - deployer/       # special usb host, use to deploy linux hosts on live machines 
      - stage           # [*] setup /deployer dir (filtered copy of base)

  - vhosts/         # virtual-machine targeted hosts (part of your git repo)
    - <host>/         # one linux virtual-machine host, goes to /deploy
    - <winhost>/      # one windows virtual-machine host (installer)
  
  - deploy<major>/  # sub-git-repo
    - init            # initiate base above (first-time setup of your git repo) 
    - bin/            # scripts launched from base (use -h for help) 
    - <distroverarchtype>/
      - make-rootfs
    - <winverarchtype>/
      - make-wininst
      
  - supersys<N>/    # sub-git-repo (used by deploy scripts)
    - install       # install to /bin

  - xdev/ # dev images to use with virtual machine (qemu/kvm), not in git!
    - <machine>.disk
    - <host>.disk
    
  - .gitignore      # filter sub-git repos, artifacts etc.  
  - setup           # run after cloning out fresh base
  - inherit         # build common files and call all <host>/inherit
  - backup          # backup to archive/<date>-<base>.7z
  - bin/            # link to deployN/bin/
```

### Inherit mechanism

- There is a common inherit script at the base `./inherit`
  - It generates: config/netlan|netpub.\<variant\> 
  - You can use it to generate other custom config
  - The common inherit script ends by calling each host's unique inherit script 
- Each host has its own inherit script `[v]hosts/<host>/inherit`
  - Use it to inherit files from:
    - deployN/generic/
    - config/ 
    - \[v\]hosts/\<host\>/ (like first host in a series of similar hosts 1,2,..)


### Passwords

- Default password shall be `123` or `Abcdef123456+`
- The default is ususally good enoght for your local "dev" environment
  - if using host local NAT-network (provided by your virtual machine)
  - if having no "dev" secrets (no rootfs.7z/rootfs.dev.7z)
  - if having "dev" secrets, but your dev environment is considered safe and encrypted-at-rest  
- When setting-up "live" environment, strap script may prompt for super-secret ADMIN password
  - keep live password in an encrypted password wallet, a safe or a very secret place
- When running host-to-ext4efi/multiefi, give your public ssh-key
  - this enables you to login using ssh (without password)


### Preparing artifacts

- Windows (prepare installer):
  - Download offical iso into artifacts/ (like Win10_21H2_English_x64.iso)
  - deployN/\<winverarchtype\>/make-wininst
- Linux rootfs I (script based):
  - deployN/\<distroverarchtype\>/make-rootfs
- Linux rootfs II (manual iso steps):
  - Download offical distro iso into artifacts/
  - deployN/bin/stage-xdisk
    - follow instructions (install iso at xdev/stage.disk)
  - deployN/bin/stage-to-rootfs


### Setup sequence for Windows

- prepare custom wininst usb-media (winhost-to-fat32inst)
- bootup usb-media
- perform the installion
- a manual process may be needed after the custom/unattended installation (create a checklist)


### Setup sequence for Linux

- `./inherit`
- dual-boot:
  - do winhost first, and later do `bin/multiefi-[luks]btrfs` with `add`, instead of `format`
- live:
  - prepare "deployer" host on usb-media (will be used to install all linux hosts)
  - bootup usb-media on target machine
  - login as root
  - establish network connection with `nic-list` and `nicip-obtain` (use -h for help)
  - `cd /deployer`
- first-time multiefi:
  - initiate disk: `bin/multiefi-[luks]btrfs <dir> <target> format|add`
- **pre-boot**:
  - `bin/host-to-<type>`  
    - (1) rootfs stage: artifacts/\<rootfs\>.rootfs.sqfs =\> /
    - (2) hosts/\<host\> to /deploy  
    - (3) write /host.conf (expanded)
    - (4) if stage script: custom stage (any to /)
    - (5) chroot: /deploy/strap 
      - install essential packages
      - prepare network (/etc/resolv.restore will become the effective resolv.conf)
      - setup ssh server (if /root/admin.pub exists)
    - (6) chroot: /deploy/sync push initial
    - (7) if boot script: source \<host\>/boot (overrides default UEFI bootloader setup)
    - (8) generate /activate script to run on target, will (re)activate bootloader
- **boot-up:**
  - login
  - run first time (as root)
    - `cd /deploy`
    - `./setup initial` # if setup exists
    - `./sync status`
    - `/activate` # re-run boot activation on target
    - `reboot`


### Syncing after deployment

- See `bin/host` and `bin/xhost` for login and remote push/pull
- Host sync-up sequence (bin/host)
  - (1) `./backup`
  - (2) `./inherit`
  - (3) `./bin/host hosts/<host> push`
  - (4) `./bin/host hosts/<host>` # login using ssh
    - (5) `cd /deploy`
    - (6) `./sync status`
    - (7) `./sync [<method>]` # or use cp 
    - (8) `./sync status`
    - (9) restart/reload services if neeeded
    - (10) `exit`
  - (11) `./bin/host hosts/<host> pull`

<hr>

### default.conf and workspace.conf
```
VARIANT="live"         # default "real" variant          
XVARIANT="dev"         # default "x" variant (using x<variant>/<name>.disk)          

ADMIN="<user>"
ADMINDIR="<dir>"
ADMINLANG="<lang>"
ADMINKB="<code>"

WINUSER="admin"
WINPASS="123"
```

### machine.conf (hardware)
```
HARDWARE="<model>"         # [*] hardware model (documentation)
UEFIKEY="<key>"            # how to bring up UEFI-menu on bootup
BOOTKEY="<key>"            # how to bring up Boot-menu on bootup

CPUTYPE="<model>"          # documentation only
RAMMIBS="<n>"              # documentation only

DISKMIBS="<n>"             # documentation only
XDISKMIBS="<n>"            # size of "x-disk" (see xdisk-create)
SWAPMIBS="<n>"             # size of shared multiefi swapfile (none if blank)

LOCATION="<location>"      # documentation only
INFO="<comment>"           # documentation only
```

### host.conf (linux)
```
MACHINE="<machine>"        # documentation: ref to machines/<machine>.ini
ROOTFS="<distroverarchtype>" # [*] artifacts/<distroverarchtype>.rootfs.sqfs

DISKMIBS="<n>"             # after-the-fact documentation, or size of virtual-machine disk
XDISKMIBS="<n>"            # size of "x-disk" (see xdisk-create)
SWAPMIBS="<n>"             # size of swapfile (none if blank)
RAMTMPMIBS="<n>"           # size of RAM /tmp, see note below (not in RAM if blank)

ADMIN="<user>"             # default: root
ADMINDIR="<dir>"           # default: /root or /home/<ADMIN>
ADMINLANG="<lang>"         # default: en_US, example: sv_SE (in addition to en_US)
ADMINKB="<code>"           # console keyboard, example: se (see XKBLAYOUT in /etc/default/keyboard)

LANDNLIST="dn1 dn2 ..."    # default: "<host>"
PUBDNLIST="dn1 dn2 ..."    # public domain name list

LANIP="n.n.n.n"            # if host has a known fixed local ip (to put in all hosts files)
PUBIP="n.n.n.n"            # if host has a known fixed public ip (to put in all hosts files) 

SSHIP="n.n.n.n"            # override LANIP/PUBIP
SSHPORT="<n>"              # default: 22

INFO="<comment>"           # documentation only
```
Note: RAMTMPMIBS must be smaller or equal to SWAPMIBS, or it will be disabled 


### winhost.conf (windows)
```
MACHINE="<machine>"        # documentation: ref to machines/<machine>.ini
WININST="<winverarchtype>" # [*] artifacts/<winverarchtype>.wininst.iso
WINKEY=""                  # if having a specific license key (must match winverarchtype)

WINMIBS="<n>"              # main-partition size (installer will ask if not given)

WINUSER="admin"            # default: admin
WINPASS="123"              # default: 123
WINLANG="en-US"            # default: en-US
WINFORMAT="<WINLANG>"      # default: same as WINLANG
WININPUT="<code>"          # see note below, default: 0409:00000409

DISKMIBS="<n>"             # after-the-fact documentation, or size of virtual-machine disk
XDISKMIBS="<n>"            # size of "x-disk" (see xdisk-create)

LANDNLIST="dn1 dn2 ..."    # default: "<host>"
PUBDNLIST="dn1 dn2 ..."    # public domain name list

LANIP="n.n.n.n"            # if host has a known fixed local ip (to put in all hosts files)
PUBIP="n.n.n.n"            # if host has a known fixed public ip (to put in all hosts files) 

SSHIP="n.n.n.n"            # override LANIP/PUBIP
SSHPORT="<n>"              # default: 22

INFO="<comment>"           # documentation only
```
Note: [Microsoft's Locale Table](win10amd64pro/winlocales.tab)

<br>
<hr>
