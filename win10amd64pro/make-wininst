#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. superlib

VARIANT="Windows 10 Pro"
LABEL="win10amd64pro"

#=================================================

# win10amd64pro/make-wininst * GPLv3 (C) 2021 librehof.com

summary()
{
  info ""
  info "Windows 10 All ISO => ${LABEL}.wininst.iso"
  info ""
  info "Usage:"
  info " ${SCRIPT} <isoname> [update|open]"
  info " ${SCRIPT} cleanup"
  info ""
  info "Input:"
  info " artifacts/<isoname>.iso"
  info " ${LABEL}/<isoname>.sha256"
  info ""
  info "Temporary:"
  info " /mnt/winiso (make and open)"
  info " /mnt/wininst (open and update existing)"
  info " ${TMPPATH}/wininst (make)"
  info ""
  info "Output:"
  info " artifacts/${LABEL}.wininst.iso"
  info ""
  info "ISO file must contain sources/install.wim"
  info "Will replace install.wim (All) with install.esd (Pro)"
  info ""
  info "Example:"
  info " ${SCRIPT} Win10_21H2_English_x64"
  info ""
}

#=================================================

TMPDIR="${TMPPATH}/wininst"

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

# check base
checkdir "artifacts"
exitonerror ${?}

if [ "${1}" = "cleanup" ]; then
  checksuperuser
  exitonerror ${?}
  ensurenodir "${TMPDIR}"
  dirmount-remove "/mnt/winiso"
  dirmount-remove "/mnt/wininst"
  exit 0
fi

#-------------------------------------------------

# input

ISONAME="${1}"
MODE="${2}"

if [ "${ISONAME}" = "" ]; then
  inputexit
fi

if [ "${MODE}" != "" ] && [ "${MODE}" != "open" ] && [ "${MODE}" != "update" ]; then
  inputexit
fi

checksuperuser
exitonerror ${?}

BASEPATH="$(pwd)"

if [ -e "/mnt/winiso" ]; then
  errorexit "Already mounted: /mnt/winiso (cleanup)"
fi

if [ -e "/mnt/wininst" ]; then
  errorexit "Already mounted: /mnt/wininst (cleanup)"
fi

ISOFILE="$(abspath "artifacts/${ISONAME}.iso")"
exitonerror ${?}

checkfile "${ISOFILE}"
exitonerror ${?}

SHAFILE="${SCRIPTPATH}/${ISONAME}.sha256"

OUTFILE="artifacts/${LABEL}.wininst.iso"

OUTFILE="$(abspath "${OUTFILE}")"
exitonerror ${?}

#-------------------------------------------------

# open

if [ "${MODE}" = "open" ]; then
  dirmount-image "/mnt/winiso" "${ISOFILE}" ro
  exitonerror ${?}
  if [ -f "${OUTFILE}" ]; then
    dirmount-image "/mnt/wininst" "${OUTFILE}" ro
    exitonerror ${?}
  fi
  info "Mounts:"
  info " /mnt/winiso"
  if [ -f "${OUTFILE}" ]; then
    info " /mnt/wininst"
  fi
  info "TODO:"
  info " dirmount-remove /mnt/winiso"
  if [ -f "${OUTFILE}" ]; then
    info " dirmount-remove /mnt/wininst"
  fi
  exit 0
fi

#-------------------------------------------------

# make

if [ -e "${TMPDIR}" ] && [ "${MODE}" != "update" ]; then
  errorexit "Already present: ${TMPDIR} (cleanup)"
fi

if [ -f "${OUTFILE}" ] && [ "${MODE}" != "update" ]; then
  errorexit "Already exists: ${OUTFILE}"
fi

ensurecmd wimlib-imagex @ wimtools
exitonerror ${?}

div

#-------------------------------------------------

note "ISOFILE: ${ISOFILE}"

if [ "${MODE}" != "update" ]; then
  div
  if [ ! -f "${SHAFILE}" ]; then
    warning "Can not verify iso (missing ${SHAFILE})"
    sleep 1
  else
    action "Calculating sha256"
    HAVE="$(shasum -a 256 "${ISOFILE}")"
    HAVE="$(spacefield "${HAVE}" 1)"
    HAVE="$(lowerstring "${HAVE}")"
    WANT="$(cat "${SHAFILE}")"
    WANT="$(lowerstring "${WANT}")"
    note "HAVE: ${HAVE}"
    note "WANT: ${WANT}"
    if [ "${HAVE}" != "${WANT}" ]; then
      errorexit "Checksum mismatch"
    fi
  fi
fi

#-------------------------------------------------
separator "ISO"

dirmount-image "/mnt/winiso" "${ISOFILE}" ro
exitonerror ${?}

div

if [ ! -f "/mnt/winiso/sources/install.wim" ]; then
  errorexit "Missing /mnt/winiso/sources/install.wim"
fi

note "Found sources/install.wim"
sleep 1

#-------------------------------------------------
separator "Prepare"

ensuredir "${TMPDIR}"
exitonerror ${?}

# temporary copy

dir-replicate "/mnt/winiso" "${TMPDIR}"
exitonerror ${?}

cp "${SCRIPTPATH}/wininst.conf" "${TMPDIR}/"
exitonerror ${?}

#-------------------------------------------------
separator "ESD"

WIMFILE="${TMPDIR}/sources/install.wim"
ESDFILE="${TMPDIR}/sources/install.esd"

if [ -f "${OUTFILE}" ] && [ "${MODE}" = "update" ]; then
  note "Reusing existing install.esd"
  dirmount-image "/mnt/wininst" "${OUTFILE}" ro
  exitonerror ${?}
  checkfile "/mnt/wininst/sources/install.esd"
  exitonerror ${?}
  cp "/mnt/wininst/sources/install.esd" "${ESDFILE}"
  exitonerror ${?}
  dirmount-remove "/mnt/wininst"
  exitonerror ${?}
else
  note "Converting install.wim into install.esd"
  wimlib-imagex export "${WIMFILE}" "${VARIANT}" "${ESDFILE}" --solid
  if [ "${?}" != 0 ]; then
    info "List variants with: wimlib-imagex info /mnt/winiso/sources/install.wim | grep \"^Name:\""
    errorexit "wimlib-imagex export ${WIMFILE} failed"
  fi
fi

ensurenofile "${WIMFILE}"
exitonerror ${?}

inheritowner "${ESDFILE}"
exitonerror ${?}

div

ls -alh "${TMPDIR}/sources/" | grep " install."
keywait 3

#-------------------------------------------------
separator "Make"

ensurecmd mkisofs @ genisoimage
exitonerror ${?}

clearfile "${STDFILE}"
exitonerror ${?}

ensurenofile "${OUTFILE}"
exitonerror ${?}

cd "${TMPDIR}"
exitonerror ${?}

mkisofs \
  -iso-level 2 \
  -J -l -D -N -joliet-long -udf \
  -relaxed-filenames -allow-limited-size \
  -b "boot/etfsboot.com" \
  -no-emul-boot -boot-load-size 8 -boot-load-seg 0x07C0 \
  -o "${OUTFILE}" \
  . 1> "${STDFILE}" 2> "${STDFILE}"

STATUS="${?}"

cd "${BASEPATH}"
exitonerror ${?}

inheritowner "${OUTFILE}"
exitonerror ${?}

notetail "${STDFILE}" 6
exitonerror ${STATUS} "Failed to make new ISO file"

div

ls -alh "artifacts/" | grep "${LABEL}.wininst.iso"

#-------------------------------------------------
separator "Cleanup"

dirmount-remove "/mnt/winiso"

if [ "${MODE}" != "update" ]; then
  ensurenodir "${TMPDIR}"
else
  div
  note "TODO: sudo rm -rf ${TMPDIR}"
fi

#-------------------------------------------------
