#!/bin/sh

IMPACT="write"
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. superlib

DISTRO="debian11"
ARCH="amd64"
RELEASE="bullseye"
SOURCE="http://ftp.us.debian.org/debian"

#=================================================

# debian11amd64strap/make-rootfs * GPLv3 (C) 2021 librehof.com

summary()
{
  info ""
  info "debootstrap => artifacts/${DISTRO}${ARCH}strap.rootfs.sqfs"
  info ""
  info "Usage:"
  info " ${SCRIPT} [--no-check-gpg]"
  info ""
  info "Mount: ${ROOTDIR}"
  info "Tmp:   ${TMPDIR}"
  info "Image: ${TMPDISK}"
  info ""
}

#=================================================

# input

ROOTDIR="/mnt/rootfs"
TMPDIR="${TMPPATH}/${DISTRO}${ARCH}cache"
TMPDISK="${TMPPATH}/${DISTRO}${ARCH}strap.disk"

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "${IMPACT}"
fi

# check base
checkdir "artifacts"
exitonerror ${?}

#-------------------------------------------------

checksuperuser
exitonerror ${?}

DEBCACHETGZ="artifacts/${DISTRO}${ARCH}strap.cache.tgz"
DEBCACHELOG="artifacts/${DISTRO}${ARCH}strap.cache.log"

ROOTFSZIP="artifacts/${DISTRO}${ARCH}strap.rootfs.sqfs"
ROOTFSLOG="artifacts/${DISTRO}${ARCH}strap.rootfs.log"

INSTALL="linux-base,locales,tzdata,binutils,dialog,debconf-utils,apt-utils,zlib1g,gzip,zip,unzip,xz-utils,lsof,nano,btrfs-progs,initramfs-tools,linux-image-amd64"

FLAGS="${1}"

if [ -e "${ROOTDIR}" ]; then
  errorexit "Allready exists: ${ROOTDIR} (dirmount-remove ${ROOTDIR})"
fi

ensurenodir "${TMPDIR}" note
exitonerror ${?}

if [ -f "${TMPDISK}" ]; then
  image-detach "${TMPDISK}"
  exitonerror ${?}
  ensurenofile "${TMPDISK}" note
  exitonerror ${?}
fi

if [ -e "${ROOTFSZIP}" ]; then
  errorexit "Already exists: ${ROOTFSZIP}"
fi

#-------------------------------------------------

# use debootstrap to setup minimal distrofs
ensurecmd debootstrap @ debootstrap
exitonerror ${?}

#-------------------------------------------------

div

note "DISTRO:  ${DISTRO} (${RELEASE})"
note "ARCH:    ${ARCH}"
note "SOURCE:  ${SOURCE}"
note "INSTALL: ${INSTALL}"
note "FLAGS:   ${FLAGS}"

div

info "Press ENTER or wait for 10 seconds"
keywait 10

ensurenofile "${ROOTFSLOG}"
exitonerror ${?}

ensurenofile "${ROOTFSZIP}"
exitonerror ${?}

#-------------------------------------------------

# cache

div

if [ -f "${DEBCACHETGZ}" ]; then

  note "Reusing ${DEBCACHETGZ}"

else

  action "Pre-strap: ${DEBCACHETGZ}"

  DEBCACHETGZ="$(abspath "${DEBCACHETGZ}")"
  exitonerror ${?}

  ensuredir "${TMPDIR}"
  exitonerror ${?}

  ensurenofile "${DEBCACHELOG}"
  exitonerror ${?}

  ensurenofile "${DEBCACHETGZ}"
  exitonerror ${?}

  debootstrap --arch "${ARCH}" --include "${INSTALL}" ${FLAGS} "--make-tarball=${DEBCACHETGZ}" "${RELEASE}" "${TMPDIR}" "${SOURCE}" > "${DEBCACHELOG}" 2> "${DEBCACHELOG}"
  STATUS=${?}

  inheritowner "${DEBCACHETGZ}"
  inheritowner "${DEBCACHELOG}"
  ensurenodir "${TMPDIR}"

  if [ "${STATUS}" -gt 1 ]; then
    notetail "${DEBCACHELOG}"
    errorexit "Failed to make debootstrap tarball (${STATUS})"
  fi

  if [ "${STATUS}" = 1 ]; then
    echo "------- status -------"
    grep "W: " "${DEBCACHELOG}"
    grep "E: " "${DEBCACHELOG}"
    echo "-------- end ---------"
    keywait 3
    FOUND="$(grep "Deleting target directory" "${DEBCACHELOG}")"
    if [ "${FOUND}" = "" ]; then
      notetail "${DEBCACHELOG}"
      errorexit "Failed to make debootstrap tarball (${STATUS})"
    fi
  fi

fi

#-------------------------------------------------

# mount

div

image-create "${TMPDISK}" 2000M
exitonerror ${?}

LOOPDEV="$(image-attach "${TMPDISK}")"
exitonerror ${?}

format-ext4 "${LOOPDEV}" "${DISTRO}"
exitonerror ${?}

dirmount-ext "${ROOTDIR}" "${LOOPDEV}"
exitonerror ${?}

#-------------------------------------------------

div

action "Strap: ${DISTRO}${ARCH}"

if [ -f "${DEBCACHELOG}" ]; then
  cp "${DEBCACHELOG}" "${ROOTFSLOG}"
  exitonerror ${?} "Failed to copy ${DEBCACHELOG}"
  appendline "" "${ROOTFSLOG}"
  exitonerror ${?}
else
  clearfile "${ROOTFSLOG}"
  exitonerror ${?}
fi

DEBCACHETGZ="$(abspath "${DEBCACHETGZ}")"
exitonerror ${?}

debootstrap --arch "${ARCH}" --include "${INSTALL}" ${OPTION} "--unpack-tarball=${DEBCACHETGZ}" "${RELEASE}" "${ROOTDIR}" "${SOURCE}" >> "${ROOTFSLOG}" 2>> "${ROOTFSLOG}"
STATUS=${?}

inheritowner "${ROOTFSLOG}"

if [ ${STATUS} != 0 ]; then
  notetail "${ROOTFSLOG}"
  errorexit "Failed to strap ${DISTRO}${ARCH} at ${ROOTDIR} (exit ${STATUS})"
fi

clearfile "${ROOTDIR}/etc/fstab"
exitonerror ${?}

ensurenofile "${ROOTDIR}/etc/crypttab" note
exitonerror ${?}

# remove resolv.conf if not a link
if [ -e "${ROOTDIR}/etc/resolv.conf" ] && [ ! -L "${ROOTDIR}/etc/resolv.conf" ]; then
  ensurenofile "${ROOTDIR}/etc/resolv.conf" note
  exitonerror ${?}
fi

#-------------------------------------------------

# zip

div

dir-squash "${ROOTDIR}" "${ROOTFSZIP}"
exitonerror ${?}

#-------------------------------------------------

# cleanup

div

dirmount-remove "${ROOTDIR}"
image-detach "${TMPDISK}"
ensurenofile "${TMPDISK}" note

#-------------------------------------------------
